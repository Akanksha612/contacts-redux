
import {useState} from "react";
import Listing from "./Components/listing_page";
import Add from "./Components/add_contact";
import "./App.css";
import { Switch, Route } from "react-router-dom";
import Editing from "./Components/Editing";
import {Get_Contacts,Add_Contact,Delete_Contact,Edit_Contact} from './Action'
import { connect } from "react-redux";

function App(props)
{

  const [data, setData]= useState( [
    {
      first_name: "Akanksha",
      last_name: "Bhardwaj",
      gender: "Female",
      dob: "2000-01-01",
      phone_number: "9650536742",
      locations: [{label:"Mumbai"},{ label:"Bangalore"}],
      image: "https://wallpapercave.com/wp/wp7826056.jpg",
      id: 10,
    },
    {
      first_name: "Ashutosh",
      last_name: "Sharma",
      gender: "Male",
      dob: "2000-02-02",
      phone_number: "9999998888",
      locations: [{label:"Goa"},{ label:"Bangalore"}],
      image: "http://essenziale-hd.com/wp-content/uploads/2017/10/patio-1-1.jpg",
      id: 11,
    }
  ],
);


props.dispatch(Get_Contacts(data));


const handleAddSubmit = (user) => {
  console.log("add submit triggered")
    console.log(user)

    props.dispatch(Add_Contact(user))
    // setData([...data, user])
 
};



const handleEditing=(modified_user)=>{

 
  let arr =[...data];

  for(let i=0;i<data.length;i++)
  {
    if(data[i].id===modified_user.id)
    arr[i]=modified_user;
  }

 setData(arr);

}




const handleDelete=(id)=>{

console.log("delete event triggered",id)
props.dispatch(Delete_Contact(id))

// let arr = data.filter((obj)=>
// {
// return obj.id !==id
// })

// setData(arr);




}












  return(

    <Switch>
    <Route
      exact
      path="/"
      render={() => (
        <Listing data={props.contacts}  handleDelete={handleDelete} />
      )}
    />
    <Route
      exact
      path="/add_user"
      render={() => <Add add_submit={handleAddSubmit} />}
    />

    <Route exact path="/editing-user/:handle"
     render={(props) => <Editing {...props}  handleEditing={handleEditing}/>}
     
    
    
    />
  </Switch>




  );
}


const mapStatetoProps=(state)=>{
  console.log(state)
  return {
    contacts: state
  }
}

export default connect(mapStatetoProps)(App);









//---------------------------------------------------------------------------------------------------------------


/*
class App extends Component {
  state = {
    data: [
      {
        first_name: "Akanksha",
        last_name: "Bhardwaj",
        gender: "Female",
        dob: "2000-01-01",
        phone_number: "9650536742",
        locations: [{label:"Mumbai"},{ label:"Bangalore"}],
        image: "https://wallpapercave.com/wp/wp7826056.jpg",
        id: 10,
      },
      {
        first_name: "Ashutosh",
        last_name: "Sharma",
        gender: "Male",
        dob: "2000-02-02",
        phone_number: "9999998888",
        locations: [{label:"Goa"},{ label:"Bangalore"}],
        image: "http://essenziale-hd.com/wp-content/uploads/2017/10/patio-1-1.jpg",
        id: 11,
      }
    ],

   
  };

  handleAddSubmit = (user) => {
    //console.log(user)
    this.setState({ data: [...this.state.data, user] }, () => {
      console.log(this.state);
    });
  };

 

  handleEditing=(modified_user)=>{

   
    let arr =[...this.state.data];

    for(let i=0;i<this.state.data.length;i++)
    {
      if(this.state.data[i].id===modified_user.id)
      arr[i]=modified_user;
    }

    this.setState({data: arr, show:false},()=>console.log("modified state", this.state.data))


  }




  handleDelete=(id)=>{

console.log("delete event triggered",id)

let arr = this.state.data.filter((obj)=>
{
return obj.id !==id
})

this.setState({data: arr})




}

  render() {
    return (
      <Switch>
        <Route
          exact
          path="/"
          render={() => (
            <Listing state={this.state}  handleSaveChanges={this.handleSaveChanges} handleDelete={this.handleDelete} />
          )}
        />
        <Route
          exact
          path="/add_user"
          render={() => <Add add_submit={this.handleAddSubmit} />}
        />
        <Route exact path="/editing-user/:handle"
         render={(props) => <Editing {...props}  handleEditing={this.handleEditing}/>}
         
        
        
        />
      </Switch>
    );
  }
}

export default App;


*/