//import React, { Component } from "react";
import React, { useState } from "react";
import { Link } from "react-router-dom";
import "../App.css";
import validator from "validator";
import Select from "react-select";
import { v4 as uuidv4 } from "uuid";

function Add(props) {
  console.log("props", props);

  const [id] = useState(uuidv4());
  const [first_name, setFirst] = useState("");
  const [last_name, setLast] = useState("");
  const [gender, setGender] = useState("");
  const [dob, setDob] = useState("");
  const [phone_number, setPhone] = useState("");
  const [locations, setLocations] = useState([]);
  const [image, setImage] = useState(null);
  const [first_name_error, setFirstError] = useState("false");
  const [last_name_error, setLastError] = useState("false");
  const [phone_error, setPhoneError] = useState("false");
  const [showErrors, setShowErrors] = useState("false");

  const options = [
    { value: "bangalore", label: "Bangalore" },
    { value: "mumbai", label: "Mumbai" },
    { value: "goa", label: "Goa" },
  ];

  const isFormValid = () => {
    console.log(first_name);
    console.log(last_name);
    console.log(gender);
    console.log(dob);
    console.log(phone_number);
    console.log(locations);
    console.log(image);
    console.log(first_name_error);
    console.log(last_name_error);
    console.log(phone_error);
    console.log(showErrors);

    return (
      first_name!==""&&
      last_name!=="" &&
      gender!==""&&
      dob!=="" &&
      phone_number!=="" &&
      image!==null &&
      locations.length !== 0 &&
      first_name_error==="false" &&
      last_name_error==="false" &&
      phone_error==="false" 
    
    );
  };

  const handleFirstName = (e) => {
    if (validator.isAlpha(e.target.value)) {
      setFirst(e.target.value);
      setFirstError("false");
      isFormValid(); //checking state
    } else {
      setFirstError("true");
      isFormValid(); //checking state
    }
  };

  const handleLastName = (e) => {
    if (validator.isAlpha(e.target.value)) {
      setLast(e.target.value);
      setLastError("false");
      isFormValid(); //checking state
    } else {
      setLastError("true");
      isFormValid(); //checking state
    }
  };

  const handleGender = (e) => {
    setGender(e.target.value);
    isFormValid(); //checking state
  };

  const handleDOB = (e) => {
    setDob(e.target.value);
    isFormValid(); //checking state
  };

  const handlePhone = (e) => {
    if (
      validator.isMobilePhone(e.target.value) &&
      e.target.value.length === 10
    ) {
      setPhone(e.target.value);
      setPhoneError("false");
      isFormValid(); //checking state
    } else {
      setPhoneError("true");
      isFormValid(); //checking state
    }
  };

  const handleLocations = (e) => {
    //console.log(Array.isArray(e)? e.map(x=> x.label):[]);

    setLocations(e);
    isFormValid(); //checking state
  };

  const handleImage = (event) => {
    if (event.target.files && event.target.files[0]) {
      let img = event.target.files[0];
      setImage(URL.createObjectURL(img));
    }

    isFormValid(); //checking state
  };

  const handleErrors = () => {
    setShowErrors("true");
    isFormValid(); //checking state
  };

  
  
  console.log("form valid", isFormValid())

  return (
    <div className="main d-flex flex-column justify-content-evenly align-items-center text-light">
      <header className="header">
        <h1>User Detail Form</h1>
      </header>
      <div className="info-container d-flex flex-column justify-content-around align-items-center">
        <form>
          <label htmlFor="fName" className="mb-1">
            <b>First Name : </b>
          </label>
          <input
            id="fName"
            type="text"
            placeholder="First Name"
            onChange={handleFirstName}
          ></input>
          {((first_name_error==="true" && showErrors==="false") || (first_name==="" && showErrors==="true")) ? 
            (<div className="text-danger error">
              Please enter correct first name
            </div>)
           : null}
          <br></br>
          <label htmlFor="lName" className="mt-3">
            <b>Last Name : </b>
          </label>
          <input
            id="lName"
            type="text"
            placeholder="Last Name"
            onChange={handleLastName}
          ></input>
          {(last_name_error === "true" && showErrors === "true") ||
          (last_name === "" && showErrors === "true") ? (
            <div className="text-danger error">
              Please enter correct last name
            </div>
          ) : null}
          <br />
          <br />
          <label htmlFor="gender" className="mt-1">
            <b>Gender:</b>
          </label>{" "}
          &nbsp; &nbsp; &nbsp; &nbsp;
          <select name="Gender" id="gender" onChange={handleGender}>
            <option defaultValue="none" selected disabled hidden>
              Choose Gender
            </option>
            <option defaultValue="female">Female</option>
            <option defaultValue="male">Male</option>
            <option defaultValue="other">Other</option>
          </select>
          {gender === "" && showErrors === "true" ? (
            <div className="text-danger error">Please choose gender</div>
          ) : null}
          <br />
          <br />
          <label htmlFor="dob">
            <b>DOB:</b>
          </label>{" "}
          &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;
          <input
            type="date"
            placeholder="Enter BirthDate"
            id="dob"
            name="birthdate"
            onChange={handleDOB}
          />
          {dob === "" && showErrors === "true" ? (
            <div className="text-danger error">Please choose date</div>
          ) : null}
          <br />
          <br />
          <label for="phone">
            <b>Phone number:</b>
          </label>
          <input
            type="tel"
            id="phone"
            name="phone"
            maxlength="10"
            onChange={handlePhone}
            placeholder="1234567890"
            pattern="[0-9]{10}"
            required
          />
          {(phone_error === "true" && showErrors === "true") ||
          (phone_number === "" && showErrors === "true") ? (
            <div className="text-danger error">
              Please enter correct phone number
            </div>
          ) : null}
          <br />
          <br />
          <br />
          <label htmlFor="locations">
            <b>Locations:</b>
          </label>{" "}
          <Select
            isMulti
            name="colors"
            options={options}
            className="basic-multi-select text-dark"
            classNamePrefix="select"
            onChange={handleLocations}
          />
          {locations.length === 0 && showErrors === "true" ? (
            <div className="text-danger error">Please choose locations</div>
          ) : null}
          <br />
          <br />
          <label htmlFor="img">
            <b className="m-2">Select image:</b>
          </label>
          <input
            type="file"
            id="img"
            name="img"
            accept="image/*"
            onChange={handleImage}
          />
          {image === null && showErrors === "true" ? (
            <div className="text-danger error">Please choose image</div>
          ) : null}
        </form>

        <div className="submit-div ">
          {isFormValid() ? (
            <Link to="/">
              <button
                className="btn btn-danger button m-5"
                onClick={() =>
                  props.add_submit({
                    first_name: first_name,
                    last_name: last_name,
                    gender: gender,
                    dob: dob,
                    phone_number: phone_number,
                    locations: locations,
                    image: image,
                    id: id,
                  })
                }
              >
                Submit
              </button>
            </Link>
          ) : (
            <button
              className="btn btn-info button m-5"
              onClick={handleErrors}
            >
              Submit
            </button>
          )}
        </div>
      </div>
    </div>
  );
}

export default Add;

/*


            <div className="submit-div ">
              {isFormValid() ? (
                <Link to="/">
                  <button
                    className="btn btn-danger button m-5"
                    onClick={() => props.add_submit(

  {

    first_name: first_name,
    last_name: last_name,
    gender: gender,
    dob: dob,
    phone_number: phone_number,
    locations: locations,
    image: image,
    id: id,



  }              )}
                  >
                    Submit
                  </button>
                </Link>
              ) : (
                <button
                  className="btn btn-danger button m-5"
                  onClick={handleErrors}

                >
                  Submit
                </button>
              )}
            </div>     


*/

//---------------------------------------------------------------------------------------------------------
/*
class Add extends Component {
  state = {
    id: uuidv4(),
    first_name: "",
    last_name: "",
    gender: "",
    dob: "",
    phone_number: "",
    locations: [],
    image: null,
    first_name_error: false,
    last_name_error: false,
    phone_error: false,

    showErrors: false,
  };

  options = [
    { value: "bangalore", label: "Bangalore" },
    { value: "mumbai", label: "Mumbai" },
    { value: "goa", label: "Goa" },
  ];

  isFormValid = () => {
    const {
      first_name,
      last_name,
      gender,
      dob,
      phone_number,
      locations,
      image,
      first_name_error,
      last_name_error,
      phone_error,
    } = this.state;

    return (
      first_name &&
      last_name &&
      gender &&
      dob &&
      phone_number &&
      image &&
      this.state.locations.length !== 0 &&
      !first_name_error &&
      !last_name_error &&
      !phone_error
    );
  };

  handleFirstName = (e) => {
    if (validator.isAlpha(e.target.value))
      this.setState({
        first_name: e.target.value,
        first_name_error: false,
      });
    else
      this.setState({
        first_name_error: true,
      });
  };

  handleLastName = (e) => {
    if (validator.isAlpha(e.target.value))
      this.setState({ last_name: e.target.value, last_name_error: false });
    else this.setState({ last_name_error: true });
  };

  handleGender = (e) => {
    this.setState({ gender: e.target.value });
  };

  handleDOB = (e) => {
    this.setState({ dob: e.target.value });
  };

  handlePhone = (e) => {
    if (validator.isMobilePhone(e.target.value) && e.target.value.length === 10)
      this.setState({ phone_number: e.target.value, phone_error: false });
    else this.setState({ phone_error: true });
  };

  handleLocations = (e) => {
    //console.log(Array.isArray(e)? e.map(x=> x.label):[]);

    this.setState({ locations: e });
  };

  handleImage = (event) => {
    if (event.target.files && event.target.files[0]) {
      let img = event.target.files[0];
      this.setState({
        image: URL.createObjectURL(img),
      });
    }
  };

  handleErrors = () => {
    this.setState({ showErrors: true });
  };

  render() {
    //console.log(validator.isDate("09/12/2021",{format: "DD/MM/YYYY"}))
    //console.log(validator.isMobilePhone("123456"))

    return (
      <div className="main d-flex flex-column justify-content-evenly align-items-center text-light">
        <header className="header">
          <h1>User Detail Form</h1>
        </header>
        <div className="info-container d-flex flex-column justify-content-around align-items-center">
          <form>
            <label htmlFor="fName" className="mb-1">
              <b>First Name : </b>
            </label>
            <input
              id="fName"
              type="text"
              placeholder="First Name"
              onChange={this.handleFirstName}
            ></input>
           
            {(this.state.first_name_error && this.state.showErrors) ||
            (!this.state.first_name && this.state.showErrors) ? (
              <div className="text-danger error">
                Please enter correct first name
              </div>
            ) : null}
            <br></br>
            <label htmlFor="lName" className="mt-3">
              <b>Last Name : </b>
            </label>
            <input
              id="lName"
              type="text"
              placeholder="Last Name"
              onChange={this.handleLastName}
            ></input>
            {(this.state.last_name_error && this.state.showErrors) ||
            (!this.state.last_name && this.state.showErrors) ? (
              <div className="text-danger error">
                Please enter correct last name
              </div>
            ) : null}
            <br />
            <br />
            <label htmlFor="gender" className="mt-1">
              <b>Gender:</b>
            </label>{" "}
            &nbsp; &nbsp; &nbsp; &nbsp;
            <select name="Gender" id="gender" onChange={this.handleGender}>
              <option defaultValue="none" selected disabled hidden>
                Choose Gender
              </option>
              <option defaultValue="female">Female</option>
              <option defaultValue="male">Male</option>
              <option defaultValue="other">Other</option>
            </select>
            {!this.state.gender && this.state.showErrors ? (
              <div className="text-danger error">Please choose gender</div>
            ) : null}
            <br />
            <br />
            <label htmlFor="dob">
              <b>DOB:</b>
            </label>{" "}
            &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;
            <input
              type="date"
              placeholder="Enter BirthDate"
              id="dob"
              name="birthdate"
              onChange={this.handleDOB}
            />
            {!this.state.dob && this.state.showErrors ? (
              <div className="text-danger error">Please choose date</div>
            ) : null}
            <br />
            <br />
            <label for="phone">
              <b>Phone number:</b>
            </label>
            <input
              type="tel"
              id="phone"
              name="phone"
              maxlength="10"
              onChange={this.handlePhone}
              placeholder="1234567890"
              pattern="[0-9]{10}"
              required
            />
            {(this.state.phone_error && this.state.showErrors) ||
            (!this.state.phone_number && this.state.showErrors) ? (
              <div className="text-danger error">
                Please enter correct phone number
              </div>
            ) : null}
            <br />
            <br />
            <br />
            <label htmlFor="locations">
              <b>Locations:</b>
            </label>{" "}
            <Select
              isMulti
              name="colors"
              options={this.options}
              className="basic-multi-select text-dark"
              classNamePrefix="select"
              onChange={this.handleLocations}
            />
            {!this.state.locations.length && this.state.showErrors ? (
              <div className="text-danger error">Please choose locations</div>
            ) : null}
            <br />
            <br />
            <label htmlFor="img">
              <b className="m-2">Select image:</b>
            </label>
            <input
              type="file"
              id="img"
              name="img"
              accept="image/*"
              onChange={this.handleImage}
            />
            {!this.state.image && this.state.showErrors ? (
              <div className="text-danger error">Please choose image</div>
            ) : null}
          </form>

          <div className="submit-div ">
            {this.isFormValid() ? (
              <Link to="/">
                <button
                  className="btn btn-danger button m-5"
                  onClick={() => this.props.add_submit(this.state)}
                >
                  Submit
                </button>
              </Link>
            ) : (
              <button
                className="btn btn-danger button m-5"
                onClick={this.handleErrors}
              >
                Submit
              </button>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default Add;

*/