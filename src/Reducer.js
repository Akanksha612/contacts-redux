import * as Actions from './Action'

export const Reducer=(contacts=[],action)=>{
    switch(action.type){
        case Actions.GET_CONTACTS:
            return [...action.contacts];

        case Actions.ADD_CONTACT:
                return [...contacts,action.contacts];

        case Actions.DELETE_CONTACT:
              console.log("reducer-delete")
                return contacts.filter(contact=> contact.id!==action.id);

        case Actions.EDIT_CONTACT:
            return contacts.map(contact=>contact.id === action.id? action.updatedContact : contact)

        default:
            return contacts;
    }
    
}