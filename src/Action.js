export const GET_CONTACTS = "GET_CONTACTS";
export const ADD_CONTACT = "ADD_CONTACT";
export const DELETE_CONTACT = "DELETE_CONTACT";
export const EDIT_CONTACT = "EDIT_CONTACT";

export const Get_Contacts=(data)=>{
    return({
        type: GET_CONTACTS,
        contacts:data
    })
}

export const Add_Contact=(newdata)=>{
    return({
        type: ADD_CONTACT,
        contacts:newdata
    })
}

export const Delete_Contact=(id)=>{
    return({
        type: DELETE_CONTACT,
        id:id
    })
}

export const Edit_Contact=(id,newdata)=>{
    return({
        type: EDIT_CONTACT,
        id:id,
        updatedContact:newdata
    })
}